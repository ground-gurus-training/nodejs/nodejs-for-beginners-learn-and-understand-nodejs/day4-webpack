import "./out/styles.css";
// import wolfgang from "./assets/images/wolfgang.jpg";

import axios from "axios";

init();

function init() {
  let x = 10;
  x += 20 + 30;
  console.log(x);
  console.log("Hello from Webpack");

  // document.querySelector("#wolfgang").src = wolfgang;

  randomDadJoke();

  document.querySelector("#btn").addEventListener("click", () => {
    randomDadJoke();
  });
}

function randomDadJoke() {
  axios
    .get("https://icanhazdadjoke.com/", {
      headers: {
        Accept: "text/plain",
      },
    })
    .then(function (response) {
      document.querySelector("#app").innerHTML = `${response.data}`;
    })
    .catch(function (error) {
      console.log(error);
    });
}
